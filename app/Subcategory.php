<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table='subcategories';
    protected $fillable=['unit_id', 'name_en', 'name_ar', 'details_en', 'details_ar', 'price', 'status',
        'currency'];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}
