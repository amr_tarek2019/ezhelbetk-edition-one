<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPackage extends Model
{
    protected $table='subscriptions_categories';
    protected $fillable=['subscription_id', 'category_id'];
}
