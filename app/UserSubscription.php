<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $table='users_subscriptions';
    protected $fillable=['user_id', 'subscription_id', 'total_price',
        'date', 'time', 'subscription_number', 'status', 'accepted','currency'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}
