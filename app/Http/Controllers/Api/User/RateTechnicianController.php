<?php

namespace App\Http\Controllers\Api\User;

use App\JobDetails;
use App\Order;
use App\OrderRequest;
use App\RateTechnician;
use App\TechnicianRate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RateTechnicianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRate(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
                 if (!$user) {
            
    return $response=[
                'success'=>403,
                'message'=>'please login first',
            ]; 
}
        $technician=JobDetails::where('order_id',$request->order_id)->pluck('technician_id')->first();
        $order=Order::where('id',$request->order_id)->first();
        $validator = Validator::make($request->all(), [
            'rate'=>'required',
            'review' => 'required',
            'order_id'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $rateUser=RateTechnician::where('order_id',$request->order_id)->orWhere('technician_id',$technician)->orWhere('user_id',$user)->exists();
        if(!$rateUser)
        {
        $technicanRate=new RateTechnician();
        $technicanRate->user_id=$user->id;
        $technicanRate->technician_id=$technician;
        $technicanRate->order_id=$order->id;
        $technicanRate->rate = $request->rate;
        $technicanRate->review = $request->review;

        $technicanRate->save();
        $response=[
            'message'=>'rate of technician created successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        }else{
        $Rate=RateTechnician::where('order_id',$request->order_id)->first();
        $Rate->rate = $request->rate;
        $Rate->review = $request->review;
        $Rate->save();
        $response=[
            'message'=>'rate updated successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        }
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
