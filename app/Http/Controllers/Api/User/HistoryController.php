<?php

namespace App\Http\Controllers\Api\User;

use App\Order;
use App\OrderSubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\JobDetails;
use App\Technician;
use App\RateTechnician;
use App\User;
use App\Subcategory;
use App\Unit;
use App\Category;


class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        if (!$user) {

            return $response=[
                'success'=>403,
                'message'=>'please login first',
            ];
        }
        $histories = Order::where('user_id', $user->id)->where('accepted', 1)
            ->where('status','6')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($histories as $res) {
            $res_item['id'] = $res->id;
            $res_item['currency'] = $res->currency;
            $res_item['created_at'] = $res->created_at;
            $res_item['status'] = $res->status;
            // $total= OrderSubcategory::select(DB::raw('sum(total) As sum'))->where('order_id',$res->id)->first();
            $total=OrderSubcategory::where('order_id',$res->id)->select('total')->sum('total');
            $res_item['total']=(string)$total;
            $res_list[] = $res_item;

        }
        //return $res_list;


        $response = [
            'message' => trans('api.get data of history successfully'),
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ActiveJobs(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        if (!$user) {

            return $response=[
                'success'=>403,
                'message'=>'please login first',
            ];
        }
        $histories = JobDetails::where('user_id', $user->id)->where('status','0')->get();
        $res_item = [];
        $res_list  = [];
        foreach ($histories as $res) {
            $res_item['id'] = $res->id;
            $res_item['order_id'] = $res->order_id;
            $res_item['status'] = $res->status;

            if(!empty($res->technician_id)) {

                $technician_id=Technician::where('id',$res->technician_id)->pluck('user_id')->first();
                $technician=User::where('id',$technician_id)->where('user_type','technician')->select('name')->first();
                $res_item['technician']=$technician->name;
            } else {
                $res_item['technician']='not assigned';
            }


            $area=JobDetails::where('id',$res->id)->pluck('order_id')->first();
            $order=Order::where('id',$area)->select('area','currency')->first();
            $res_item['area']=$order->area;
            $res_item['currency']=$order->currency;

            $subcategoryData=JobDetails::where('id',$res->id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('id',$subcategoryData)->pluck('subcategory_id')->first();
            $subcategory = Subcategory::where('id',$subcategoryId)->select('name_'.$lang.' as name')->first();
            $res_item['subcategory']=$subcategory->name;


            $subcategoryTotal=OrderSubcategory::where('id',$subcategoryData)->select('total')->first();
            $res_item['total'] = $subcategoryTotal->total;


            $cat=JobDetails::where('id',$res->id)->pluck('order_id')->first();
            $unit=OrderSubcategory::where('id',$subcategoryData)->pluck('subcategory_id')->first();
            $category=Subcategory::where('id',$unit)->pluck('unit_id')->first();

            $unit=Unit::where('id',$category)->pluck('category_id')->first();

            $categoryName=Category::where('id',$unit)->select('name_'.$lang. ' as name','icon')->first();

            $res_item['category'] = $categoryName->name;
            $res_item['icon'] = $categoryName->icon;

            $res_list[] = $res_item;

        }



        $response = [
            'message' => trans('api.get data of history successfully'),
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jobDetails=Order::where('id',$request->order_id)->get();

        $res_item = [];
        $res_list  = [];
        foreach ($jobDetails as $res) {
            $res_item['id'] = $res->id;

            $technicianId=JobDetails::where('order_id',$request->order_id)->pluck('technician_id')->first();

            $technician_id=Technician::where('id',$technicianId)->pluck('user_id')->first();

            $technician=User::where('id',$technician_id)->where('user_type','technician')->select('lat','lng','name','phone','image','address')->first();

            $res_item['technician']=$technician;

            $subcategoryData=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
            $subcategoryId=OrderSubcategory::where('id',$subcategoryData)->pluck('subcategory_id')->first();

            $subcategory = Subcategory::where('id',$subcategoryId)->select('name_'.$lang. ' as name','price')->get();


            $res_item['subcategory']=$subcategory;

            $orderId=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
            $orderData=Order::where('id',$orderId)->select('area','block','street','house')->first();


            $res_item['order_details'] = $orderData;


            $job=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
            $order=Order::where('id',$job)->select('payment_type','currency')->first();
            $res_item['payment_type'] = $order->payment_type;
            $res_item['currency'] = $order->currency;

            $dateAndTime=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
            $orderDateAndTime=Order::where('id',$job)->select('date','time')->first();
            $res_item['date'] = $orderDateAndTime->date;
            $res_item['time'] = $orderDateAndTime->time;

            $techId=JobDetails::where('order_id',$request->order_id)->pluck('technician_id')->first();



            $techRate=RateTechnician::where('technician_id',$techId)->select('rate')->avg('rate');
            $res_item['technicianRate']=(string)$techRate;


            $totalPrice=JobDetails::where('order_id',$request->order_id)->pluck('order_id')->first();
            $total=OrderSubcategory::where('order_id',$totalPrice)->select('total')->first();
            $res_item['total']=$total->total;



            $res_list = $res_item;
        }
        $response = [
            'message' =>'get data of job successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
